# Source Viewer Playground

This is a watered-down version of the GitLab repository Source Viewer.

The project only contains the Source Viewer and essential dependencies such as Highlight.js.

It is intended to be used as a playground for prototyping new features or performance 
improvements without having to setup the entire GitLab project.

A basic debugger can be enabled by adding `?debugger=true` to the URL.

**Note: The project is not actively maintained and very likely to be out of sync with the current
Source Viewer in GitLab.**

### Getting started

1. Run `yarn`
2. Run `yarn run dev`
