import { setFileSize } from "./utils/local_storage";

const files = {
  50: import("./files/50.json?raw"),
  100: import("./files/100.json?raw"),
  "1k": import("./files/1k.json?raw"),
  "30k": import("./files/30k.json?raw"),
  "100k": import("./files/100k.json?raw"),
};

export async function loadBlob(size) {
  console.log("loadBlob: ", size);

  setFileSize(size);
  const file = await files[size || 100];

  return {
    rawTextBlob: file.default,
    blamePath: "",
    language: "javascript",
  };
}

export const sizes = Object.keys(files);
