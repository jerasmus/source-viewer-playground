export const FILE_SIZE_KEY = 'fileSize';
export const LCP_KEY = 'lcp_min_';

export function getFileSize() {
    return localStorage.getItem(FILE_SIZE_KEY);
}

export function setFileSize(value) {
    return localStorage.setItem(FILE_SIZE_KEY, value);
}

export function clearLocalStorage() {
    return localStorage.clear();
}

export function setLCP(entry, value) {
    return localStorage.setItem(`${LCP_KEY}${entry}`, value);
}

export function getLCP(entry) {
    return localStorage.getItem(`${LCP_KEY}${entry}`);
}

export function addLCPEntry(newEntry) {
    const minOne = getLCP('one');
    const minTwo = getLCP('two');
    const minThree = getLCP('three');

    setLCP('one', newEntry);
    setLCP('two', minOne);
    setLCP('three', minTwo);
    setLCP('four', minThree);
}

export function getLCPEntries() {
    return [getLCP('one'), getLCP('two'), getLCP('three'), getLCP('four')]
}