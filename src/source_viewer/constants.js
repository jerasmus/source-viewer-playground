export const LINES_PER_CHUNK = 70;

export const BIDI_CHARS = [
  '\u202A', // Left-to-Right Embedding (Try treating following text as left-to-right)
  '\u202B', // Right-to-Left Embedding (Try treating following text as right-to-left)
  '\u202D', // Left-to-Right Override (Force treating following text as left-to-right)
  '\u202E', // Right-to-Left Override (Force treating following text as right-to-left)
  '\u2066', // Left-to-Right Isolate (Force treating following text as left-to-right without affecting adjacent text)
  '\u2067', // Right-to-Left Isolate (Force treating following text as right-to-left without affecting adjacent text)
  '\u2068', // First Strong Isolate (Force treating following text in direction indicated by the next character)
  '\u202C', // Pop Directional Formatting (Terminate nearest LRE, RLE, LRO, or RLO)
  '\u2069', // Pop Directional Isolate (Terminate nearest LRI or RLI)
  '\u061C', // Arabic Letter Mark (Right-to-left zero-width Arabic character)
  '\u200F', // Right-to-Left Mark (Right-to-left zero-width character non-Arabic character)
  '\u200E', // Left-to-Right Mark (Left-to-right zero-width character)
];

export const BIDI_CHARS_CLASS_LIST = 'unicode-bidi has-tooltip';

export const BIDI_CHAR_TOOLTIP = 'Potentially unwanted character detected: Unicode BiDi Control';

export const HLJS_ON_AFTER_HIGHLIGHT = 'after:highlight';

// We fallback to highlighting these languages with Rouge, see the following issue for more detail:
// https://gitlab.com/gitlab-org/gitlab/-/issues/384375#note_1212752013
export const LEGACY_FALLBACKS = ['python'];
