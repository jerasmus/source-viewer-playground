import { fileURLToPath, URL } from 'node:url'

import { defineConfig } from 'vite'
import legacy from '@vitejs/plugin-legacy'
import vue2 from '@vitejs/plugin-vue2'
import vitePluginRaw from 'vite-plugin-raw';
const path = require('path');

// https://vitejs.dev/config/
export default defineConfig({
  base: '/source-viewer-playground/',
  plugins: [
    vitePluginRaw({
      match: /\.json$/,
      exclude: [new RegExp(path.resolve(__dirname, './src/assets'))]
    }),
    vue2(),
    legacy({
      targets: ['ie >= 11'],
      additionalLegacyPolyfills: ['regenerator-runtime/runtime']
    }),
  ],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
    }
  }
})
